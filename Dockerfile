ARG BASE_IMAGE="php:cli-alpine"

# Set the base image for subsequent instructions
FROM $BASE_IMAGE

# Update packages
RUN apk update

# Install PHP and composer dependencies
RUN apk --no-cache add git libxml2-dev mysql-client zip unzip bash libmcrypt-dev libltdl

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-configure mcrypt
RUN docker-php-ext-install mbstring pdo_mysql soap zip opcache mcrypt

# Install & enable Xdebug for code coverage reports
RUN apk add --no-cache $PHPIZE_DEPS && pecl install xdebug && docker-php-ext-enable xdebug

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
